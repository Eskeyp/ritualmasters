from django.apps import AppConfig


class AuthConfig(AppConfig):
    name = 'rmaster.account'
