from .base import *

STATIC_URL = '/static/'
MEDIA_URL = '/media/'

HOST = 'localhost:8080'

SECRET_KEY = 've5wow*i5oropa3eyo_rygw84l!pl59&*br&&_an&62_v!y*#9'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'ritulamastes',
        'USER': 'rmaster',
        'PASSWORD': 'ritual',
        'HOST': 'localhost',
        'PORT': '',
    }
}

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

EMAIL_HOST = 'localhost'

EMAIL_PORT = 1025

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, '../../frontend/static'),
)
